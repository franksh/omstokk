"use strict";

var DEBUG = false;
// DEBUG = true;

/*
 * CAVEAT: I am not a web person. I am both in awe of--and despise--web
 * programming. I have no idea what I'm doing. I feel like a monkey duct taping
 * together advice from StackOverflow.
 * 
 */

/*
 * Log (sort of):
 *
 * *** Iteration 0:
 *
 * unknown number of hours (half a day?).
 *
 * Failed attempt at trying to use/learn Flutter/Dart.
 * c.f. http://franksh.gitlab.io/rants/primordial-web.html
 *
 * Only got to the drag&drop stage but then started to run into problems and
 * spent more time Googling than I did thinking.
 *
 * *** Iteration 1:
 *
 * half a day's work, started from scratch with index.html and
 * jQuery. Much easier. No game logic. Just making sure drag&drop + keyboard
 * input was working as I intended.
 *
 * *** Iteration 2:
 *
 * static wordpile-solution sets and basic game logic. Too many
 * hours blindly fumbling with CSS to make it look 2% less shit. Much
 * inefficient.
 *
 * *** Iteration 3:
 *
 * counters for score, round, timer for each round, etc. Quarter of
 * a day's work.
 *
 * *** Iteration 4:
 *
 * better anagram generation (see ana.py), which was probably the most fun part
 * yet. Mapping letters to primes and anagram sets to natural numbers.
 * Difficulty setting, more variation in word length. Game is approaching some
 * functional minimum. Another quarter of a day's work.
 *
 * *** Iteration 5:
 *
 * countless hours, a day's work: tons of Googling, ADHD, failed experiments,
 * whatever. Tried getting rid of the ui-sortable hack on the target slots.
 * Successfully got rid of the drag-event-simulate script. Ideally I'd like to
 * genealize--to have reuseable components, to make things simple and elegant.
 * Ideally: the rack/target "concepts" ought to be simple and uncomplicated;
 * they should "just work" when instantiated on some DOM element. Their state
 * should be self-govererned, their interactions well-defined... But since I'm
 * a headless chicken stuck in front of a computer...
 *
 * *** Iteration 6:
 *
 * Converting it to being supported by a backend for highscores and the like.
 * Two days work at least. Most of which was spent Googling. I sort of hate
 * Flask's documentation now.
 *
 */

/*
 * Story time: (outdated)
 *
 * In my youth the first programming language I used to any serious degree was
 * probably Visual Basic 1.0.
 *
 * Me and my friend both started programming and we were awful at it. I was
 * obsessed with loops and subroutines and wrote everything out as my own
 * subroutines with loops whenever I could. Screw the built-in methods, I can
 * count the length myself, thank-you-very-much.
 *
 * And my friend didn't quite grasp the concept of variables that persistend
 * beyond the scope of these "subroutines": instead he would store all his
 * persistent data in hidden text input widgets -- not actually hidden, but
 * positioned hundreds of pixels to the left or bottom, so they wouldn't be
 * visible for the user but you could find them if you made the window big
 * enough. He would then do his calculations directly on the strings stored in
 * these widgets.
 *
 * I think he was way ahead of his time. He was programming for the web and
 * mobiles when I was programming for the past.
 *
 * So this little web game is written in his honor: screw having any sort of
 * "model" or abstraction on the program state. We'll just use the UI directly!
 */

/*
 * Random note:
 *
 * There's many parameters that can be varied. Most of them are largely
 * invisible. These are not "variants of the game", as they do not change any
 * fundamental mechanic, but rather just "neighborhoods" of the game in the
 * space of games. They subtly affect the 'feel' of the game yet it is NOT
 * something most users will want to modify themselves.
 *
 * - variance in length of words
 *
 * later rounds should probably give longer words? at what distribution? should
 * short words stop appearing at all at one point?
 *
 * - 'difficulty' of words or letter sets
 *
 * hard to measure, but going by # of words that can be made from a collection
 * of letters seems good enough for now.
 *
 * this difficulty should probably be going up as the rounds increase, but
 * again: at what rate?
 *
 * Notes:
 *
 * words that start with vowels are much harder though (in my experience).
 *
 * compound words are probably the most difficult of all.
 *
 * words ending with -ing, -er, etc tend to be easy.
 *
 * - time per round
 *
 * later rounds should probably get faster? at what rate? and how is score tied
 * to time?
 *
 * - etc etc etc
 * 
 */

// For typing in name on high score screen.
// TODO: extract from html instead, to make language agnostic?
var alphabet = ' ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ!?❤';

// TODO: extract from html instead, to make language agnostic?
var difficulties = [
  'Nådeløs 😈',
  'Vanskelig 😬',
  'Barsk 😐',
  'Lett 😀',
];

// high score tables.
$(function() {
  // cache for tabs.
  $("#highscores").tabs({
    beforeLoad: function(event,ui) {
      if (ui.tab.data("loaded")) {
        event.preventDefault();
        return;
      }
      ui.jqXHR.done(function() {
        ui.tab.data("loaded", true);
      }).fail(function() {
        ui.panel.text("Failed to load table.")
      });
    },
  });
})

// convenience method to post json in jquery.
$.extend({
  post_json: function(url, data, cb) {
    return $.ajax({
      url: url,
      type: "POST",
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify(data),
      success: cb,
    });
  },
});

// the little login-box with the current name.
function recheck_my_name() {
  if (my_name) {
    $("#my-name").text(my_name);
    $("#login-info").show().effect("highlight", {}, 'slow');
  } else {
    $("#login-info").hide();
  }
}

$(function() {
  $("#login-info button").on('click', function() {
    my_name = null;
    $.post_json('/omstokk/reset-name', {})
    recheck_my_name();
  });
  recheck_my_name();
})


/*
 * stealing code from stackoverflow: I'm a real programmer now.
 *
 * (ended up not using this anyway... yet.)
 */
$.fn.extend({
  // Modified and Updated by MLM
  // Origin: Davy8 (http://stackoverflow.com/a/5212193/796832)
  animateAppendTo: function(newParent, duration) {
    duration = duration || 'slow';

    var $element = $(this);

    newParent = $(newParent); // Allow passing in either a JQuery object or selector
    var oldOffset = $element.offset();
    $(this).appendTo(newParent);
    var newOffset = $element.offset();

    var temp = $element.clone().appendTo('body');

    temp.css({
      'position': 'absolute',
      'left': oldOffset.left,
      'top': oldOffset.top,
      'zIndex': 1000
    });

    $element.hide();

    temp.animate({
      'top': newOffset.top,
      'left': newOffset.left
    }, duration, function() {
      $element.show();
      temp.remove();
    });
  }
});



/*
 * Some utility stuff working with offets.
 *
 * Not really needed anymore(?). I used them for drag and drop stuff.
 */
function offset_mul(s,a) {
  return {
    top: a.top * s,
    left: a.left * s,
  };
}
function offset_add(a,b) {
  return {
    top: a.top + b.top,
    left: a.left + b.left,
  };
}

$.fn.extend({
  outerSize: function() {
    return {
      top: this.outerHeight(),
      left: this.outerWidth(),
    };
  },
  innerSize: function() {
    return {
      top: this.innerHeight(),
      left: this.innerWidth(),
    };
  },
  centerOffset: function() {
    return offset_add(this.offset(), offset_mul(0.5, this.outerSize()));
  },
});



/*
 * This is really ugly. It just pushes a lot of random logic/functionality over
 * to jQuery side. This makes the UI elements more ergonomic to work with,
 * but...
 *
 * I've seen there's various ways for creating 'jq subclasses' but none of them
 * felt very natural...
 *
 * Another option is $.widget(), which I'd ideally want to do for things like
 * "rack" and "target", but that carries with it a lot of state and
 * weirdness I'm not prepared to deal with for something so simple.
 */

$.fn.extend({
  check_full: function() {
    if (this.find('.drop-target').length === 0) {
      this.trigger('omstokk_full', this.block_word());
    }
    return this;
  },
  first_empty: function() {
    return this.find(':empty').first();
  },
  find_blocks: function(l) {
    var reg = new RegExp(l, "i");
    return this.find('.letter-block').filter(function() { return reg.test($(this).text()); });
  },
  block_word: function() {
    return this.find('.letter-target > .letter-block > .letter-text').text();
  },
  last_block: function() {
    return this.find('.letter-block').last();
  },
  make_block: function(t, sc) {
    return this.empty()
      .addClass('letter-block')
      .append($('<span>').addClass('letter-text').text(t))
      .append($('<span>').addClass('score-text').text(sc))
  },
  make_drop_target: function() {
    return this.addClass(['letter-target', 'drop-target']);
  },
  target_push: function(block) {
    // I am the slot container.
    var slot = this.first_empty();
    slot.append(block);
    slot.removeClass('drop-target');
    slot.sortable('refresh');
    this.check_full();
    return this;
  },
  slot_clear: function() {
    // I am the slot, returning a block.
    var el = this.find('.letter-block').last();
    el.remove();
    this.sortable('refresh');
    this.addClass('drop-target');
    return el;
  },
  target_pop: function() {
    // I am the slot container, returning a block.
    var el = this.find('.letter-block').last();
    var slot = el.parent();
    return slot.slot_clear();
  },
  make_target: function(len) {
    this.empty();
    _.range(len).forEach(i => $('<div>').make_drop_target().appendTo(this));
    this.find('.drop-target').sortable({
      placeholder: 'placeholder-target',
      forcePlaceholderSize: true,
      tolerance: 'intersect',
      connectWith: ['#rack', '.drop-target'],
      receive: (evt,ui) => {
        this.trigger('omstokk_change');
        $(evt.target).removeClass('drop-target');
        this.check_full();
      },
      remove: (evt,ui) => {
        $(evt.target).addClass('drop-target');
      },
    });
    return this;
  },
  make_rack: function(letterset) {
    this.empty();
    _.shuffle(letterset).forEach((l,i) => {
      $('<div>').make_block(l, 1).appendTo(this);
    });
    this.sortable({
      tolerance: 'intersect',
      connectWith: '.drop-target',
      placeholder: 'placeholder-rack',
      forcePlaceholderSize: true,
    });
    return this;
  },
  make_kb: function(letterset) {
    letterset.forEach((l,i) => {
      $('<div>').make_block(l, 1).appendTo(this);
    });
    this.find('.letter-block').draggable({
      helper: 'clone',
      connectToSortable: '.drop-target',
    });
    return this;
  }
});


/*
 * Old round class repurposed to just be a timer/progress bar.
 *
 */
class OmstokkTimer {
  constructor(el, left) {
    left = left ? left : 60.0;
    this.el = $(el).progressbar({
      max: left,
      value: left,
      change: () => {
      },
    });
    this.el.find('.progress-label').text(left.toFixed(1) + ' sek.');
    this.end = null;
    this.running = false;
  }

  start() {
    if (this.running) { this.stop(); }
    this.end = performance.now() / 1000.0 + this.el.progressbar('value');
    this.running = true;
    this.update_time();
  }

  stop() {
    if (!this.running) { return; }
    this.running = false;
    this._update(); // one last update.
  }

  _update() {
    var left = this.end - performance.now() / 1000.0;
    this.el.progressbar('value', left);
    this.el.find('.progress-label').text(left.toFixed(1) + ' sek.');
    return left;
  }

  update_time() {
    if (!this.running) { return; }
    var l = this._update();
    if (l < 0) {
      this.stop();
      this.el.trigger('omstokk_timeout');
      return;
    }
    setTimeout(() => {
      this.update_time();
    }, 200); // the argument order of setInterval/setTimeout is so oof.
  }

  add_time(delta) {
    var opts = this.el.progressbar('option');
    this.el.progressbar('option', {
      max: opts.value + delta,
      value: opts.value + delta,
    });
  }
}


// Difficulty-specific game.
class Game {
  constructor(d) {
    this.div = $("#omstokk");
    this.div.find('#gameover').empty().hide();
    this.div.find('#info,#running,#progress').show();
    this.div.find('#extra-msg').hide();
    this._score = this.div.find('#score');
    this._score.text('0');
    this.div.find('#round').text(DEBUG ? '10' : '');

    this.words = [];
    this.accepted = [];
    this.difficulty = d;
    this.progress = new OmstokkTimer('#progress', 60);
    this.rack = null;
    this.target = null;

    // this switch is an ugly hack. It says whether we're showing the special
    // "please enter your initials" rack or not.
    this.kb_mode = false;

    if (DEBUG) {
      $('#debug_kb').off('click');
      $('#debug_kb').on('click', () => {
        this.div.trigger('omstokk_timeout');
      });
      $('#debug_timeout').off('click');
      $('#debug_timeout').on('click', () => this.div.trigger('omstokk_timeout'));
    }

    this.div.on('omstokk_full', (evt, data) => this.test_input(data));
    this.div.on('omstokk_timeout', (evt) => this.game_over());
    this.div.on('omstokk_change', () => {
      if (this.round() === 1 && !this.progress.running && !this.kb_mode) {
        this.progress.start();
      }
    });

    $(document).on('keydown', (evt) => {
      // console.log('keydown', evt.key);
      if (evt.ctrlKey || evt.altKey) {
        return true;
      }
      var from = [];
      var to = [];
      if (evt.key == 'Backspace' || evt.key == 'Delete') {
        from = this.target.target_pop();
        if (from.length === 1 && !this.kb_mode) {
          this.rack.append(from);
          this.rack.sortable('refresh');
        }
      } else {
        from = this.rack.find_blocks(evt.key).first();
        if (from.length === 1) {
          if (this.kb_mode) {
            this.target.target_push(from.clone());
          } else {
            this.target.target_push(from);
            this.rack.sortable('refresh');
            this.div.trigger('omstokk_change');
          }
        }
      }
      return false;
    });

    $('body').on('click', '.letter-target', (evt) => {
      var from = $(evt.currentTarget).slot_clear();
      if (from.length === 1 && !this.kb_mode) {
        this.rack.append(from);
        this.rack.sortable('refresh');
      }
      return false;
    });

    this.next_round();
  }

  // Schedule next round and fetch more data if we're running low.
  //
  // Next round happen immediately if there's data avilable.
  next_round() {
    var rd = this.round();
    if (this.words.length < rd + 3) {
      var url = '/omstokk/ls/get?' + new URLSearchParams({
        rd: this.words.length + 1,
        n: 5,
        d: this.difficulty,
      });

      var promise = fetch(url).then(r => r.json()).then(d => this.words.push(...d));

      if (this.words.length <= rd) {
        promise.finally(() => this.next_round());
        return;
      }
    }

    // assert this.words.length > rd
    this.reinit();
  }

  round() {
    return this.div.find('#round').text()|0; // hehe
  }

  score() {
    return this._score.text()|0; // hehe
  }

  test_input(data) {
    if (this.kb_mode) {
      // $('#progress').empty(); // TODO: hack
      my_name = data;
      recheck_my_name();
      this.register_hs(data);
      return;
    }
    if (!this.progress.running) { return; }
    if (_.includes(this.accepted, data)) {
      // TODO: a most terrible hack just so that I don't have
      // to mess with the SQL/server. The client provides all
      // the data currently and the server trusts entirely.
      this.words[this.round()-1].push(data, 2*data.length);

      this.add_score(2, data);
      this.div.hide({
        effect: 'slide',
        direction: 'right', //options: {'direction': 'left'},
        duration: 'fast',
        complete: () => this.next_round(),
      });
    } else {
      this.add_score(-1, null);
      this.div.effect('bounce', {
        'times': 3,
        'distance': this.div.outerHeight() / 10,
      }, 'medium');
    }
  }

  add_score(n, word) {
    var sc = this.score();
    if (n <= 0) {
      sc = Math.max(0, sc+n);
    } else {
      sc += n * word.length; // TODO: figure scoring.
      this.progress.stop();
      this.progress.add_time(word.length * 2);
    }
    this._score.text(sc);
    this._score.effect('highlight');
  }

  reinit() {
    this.div.hide();
    this.div.find('.letter-block').remove();
    this.div.find('.letter-target').remove();

    var rd = this.round();
    if (rd > 0) {
      this.progress.start();
    }
    
    // assert this.words[rd]
    var wl = this.words[rd][0].split(':');
    var len = parseInt(wl[0]);
    var ls = _.shuffle(wl[1]);
    this.accepted = this.words[rd][1]

    this.target = this.div.find('#target').make_target(len);
    this.rack = this.div.find('#rack').make_rack(ls);

    $('#round').text(rd+1);

    if (DEBUG) {
      $('#debug_sln').remove();
      $('<span>').attr('id', 'debug_sln').text(this.accepted.join(' ')).appendTo('#controls');
    }

    this.div.show({
      effect: 'slide',
      duration: 'fast',
      direction: 'left',
    });
  }

  game_over() {
    if (this.progress.running) {
      this.progress.stop();
    }
    this.div.find('.letter-block').remove();
    this.div.find('.letter-target').remove();

    if (my_name === null) {
      this.div.find('#progress').hide({
        effect: 'bounce',
        duration: 'slow',
        complete: () => {
          // TODO: hack
          // $(this).progressbar('destroy').text('Vennligst stav initialene dine, slik at du kan havne på high score lista.').show('fade');
          this.div.find('#extra-msg').show('fade');
        },
      });
      this.kb_mode = true;
      this.target = this.div.find('#target').make_target(3);
      this.rack = this.div.find('#rack').sortable('destroy').make_kb(alphabet.split(''));
    } else {
      this.register_hs(my_name);
    }
  }

  register_hs(name) {
    this.div.find('#running').hide({
      effect: 'fade',
      duration: 'fast',
    });
    this.div.find('#extra-msg').hide();
    $('#gameover').empty();
    $.post_json('/omstokk/hs/post', {
      name: name,
      score: this.score(),
      difficulty: this.difficulty,
      history: this.words.slice(0, this.round()), // TODO: hack
    }, (debrief) => {
      // console.log('got response', debrief);
      $('#gameover').html(debrief).show();
      $('#highscores > ul > li').data('loaded', false);
      $('#highscores').tabs('load', $('#highscores').tabs('option', 'active'));
    });
  }

  kill() {
    this.progress.stop();
    this.div.find('.letter-block').remove();
    this.div.find('.letter-target').remove();
    this.words = null;
    this.rack = null;
    this.target = null;
    $(document).off('keydown');
    this.div.off();
  }
}


// Access to GAME outside this function callback hell.
var GAME = null;

function start_game(d) {
  if (GAME) {
    GAME.kill();
  }
  $('#omstokk > *').hide();
  $('#difficulty').text(difficulties[d]); // hack
  GAME = new Game(d);
}

$(() => {
  $('#controls button').on('click', function (evt) {
    start_game( parseInt($(this).attr('data-level')) );
  });

  // Debug.
  if (DEBUG) {
    $('<button>').attr('id', 'debug_timeout').text('timeout').appendTo($('#controls'));
    $('<button>').attr('id', 'debug_kb').text('show kb').appendTo($('#controls'));
  }

  start_game(3);
});

