from collections import Counter, defaultdict
from itertools import product, combinations
from pathlib import Path
import pickle
from math import lcm, gcd, prod
import random
import io
from bisect import bisect
import json
import math

import numpy as np

# There's a lot of algorithms, but I don't really have the motivation to go into
# full exploration mode. Operating directly with strings seems to be the simplest.
#
# The clever trick of mapping to primes/integers would only bring a benefit if
# granted a full rewrite to numpy.
#
# As for the generation of anagram sets...
#
# 1. The naive way: just pick a random set of letters and count possible words.
#
# The fear is that this will give an 'unnatural' looking sets or a skewed
# distribution. Another big problem is that it will generate sets that has
# "dead" characters in it (that aren't used in any word).
#
# 1ex. (variant) Start with a random word and add a handful of random letters
# directly.
#
# Although slightly better than (1), it still has the same flaws.
#
# 2. Start with a base word/anagram set (anagram of a valid L-length word) and
# add letters one by one, making sure each new letter is used.
#
# This is what is implemented below, replacing the much slower (3).
#
# One problem here is that certain anagram sets might not be reachable unless
# adding two or more letters.
#
# 3. Start with a base L-length word, calculate all other L-length sets
# reachable from this set by adding/removing any alphabet character. Select
# among these sets based on how many new allowable words they add.
#
# Not sure about the merits of this technique. It is what I started out with, as
# it ought to generate natural-looking letter sets. However, it might turn out
# that some simple modification of (2) that doesn't add complexity can be shown
# to generate essentially the same sets.

# Latest version just uses simple, straightforward functions:

def ana(s):
  "Normalize to an anagram set."
  return ''.join(sorted(s))

def anagrams_1(s, c):
  """Returns the anagram sets that can be formed from `s` by taking away one
  character and replacing it with `c`.

  Does not include `s` itself. Assums `s` is already normalized.

  Yeah, I'm aware of how the code looks.

  """
  i = bisect(s, c)
  return tuple(
    s[:i] + c + s[i:k] + s[k+1:]
    if i <= k else
    s[:k] + s[k+1:i] + c + s[i:]
    for k in range(len(s))
    if s[k] != c and (k == 0 or s[k] != s[k-1])
  )

def anagrams_of(coll, s, n):
    return frozenset(u for u in (''.join(x) for x in combinations(ana(s), n)) if u in coll)

def anagram_words(coll, s, n):
  return set.union(*(coll[ls] for ls in anagrams_of(coll, s, n)))

def count_words(coll, s, n):
  return sum(coll[ls] for ls in anagrams_of(coll, s, n))

def gen_anasets(state, L, softmax=20, hardmax=60):
  ws = [x for x in state.words if len(x) == L]
  cnts = {k: len(v) for k,v in state[L].items()}

  alphabet = list(state.alphacount.keys())
  alphaweight = np.array(list(state.alphacount.values()), dtype=np.float64)
  alphaweight **= 0.7  # smooth out the probability curve so it's less top-heavy.
  alphaweight /= alphaweight.sum()

  while True:
    s = ana(random.choice(ws))
    c = cnts[s]
    yield (s, c)
    if c > softmax:
      continue
    for _ in range(L//3 + 2):
      l = random.choices(alphabet, weights=alphaweight, k=1)[0]
      c_ = count_words(cnts, s+l, L)
      assert c_ >= c
      if c_ == c or c_ >= hardmax:
        continue
      c = c_
      s = ana(s+l)
      yield (s, c)
      if c >= softmax or len(s) - L >= math.ceil(L/6+1):
        break



# Useful classes for working with precalculated data. Ripped off from some
# private libraries, here in ad-hoc form.

def load_or_build(fname, func):
  """Load or build a cached object using `pickle`."""
  if not fname.is_file():
    print(f"cache file '{fname}' not found: rebuilding")
    obj = func()
    pickle.dump(obj, fname.open('wb'))
  else:
    print(f"loading cache from '{fname}'")
    obj = pickle.load(fname.open('rb'))
  return obj


class jsstate(dict):
  """Utility class extracted from my `flagmining` library."""
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.__dict__ = self # The key trick, so this object can be used like a namespace object.
  def __getattr__(self, key):
    return self.__getitem__(key)
  def _filename(self, key):
    raise NotImplementedError("override this")
  def __getitem__(self, key):
    if key in self:
      return super().__getitem__(key)
    if isinstance(key, str):
      assert not key.startswith('_')
      loader = getattr(self, f'_{key}')
    else:
      loader = lambda: self._item(key)
    obj = load_or_build(self._filename(key), loader)
    self[key] = obj
    return obj


class ana_state(jsstate):
  def _filename(self, key):
    return self.filename.with_suffix(f'.{key}.p')

  def _words(self):
    with self.filename.open('rt') as fil:
      return frozenset(l for l in (l.strip().lower() for l in fil) if l)

  # def _alphabet(self):
  #   return prime_alphabet(''.join(l for l,_ in Counter(''.join(self.words)).most_common()))

  def _alphacount(self):
    return Counter(''.join(self.words))

  def _item(self, L):
    t = dict()
    for w in filter(lambda x: len(x) == abs(L), self.words):
      if L < 0:
        t.setdefault(self.alphabet(w), set()).add(w)
      else:
        t.setdefault(ana(w), set()).add(w)
    return t

  # def choices(self, L):
  #   return ana_choices((k,len(v)) for k,v in self[L].items())



# primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37,
#           41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83,
#           89, 97, 101, 103, 107, 109, 113, 127, 131,
#           137, 139, 149, 151, 157, 163, 167, 173]

# _primeset = frozenset(primes)
# _primeset2 = frozenset(x*y for x,y in product(primes, repeat=2))

# class prime_alphabet(str):
#   """Mapping between multisets of an alphabet and the natural numbers.

#   This mapping uses exponents of the prime numbers to give an injective
#   function. For example the symbols {a,b,c} are represented as the primes
#   {2,3,5}, so the multiset {a,a,c} can be represented uniquely by 2^2*5^1.

#   """
#   def __call__(self, word):
#     return prod(primes[self.index(l)] for l in word)

#   def to_str(self, n):
#     """Map a number back to """
#     res = ''
#     for i,(c,p) in enumerate(zip(self, primes)):
#       while n % p == 0:
#         res += c
#         n //= p
#       if n == 1:
#         return res
#     raise ValueError(f"not all primes accounted for, {n} != 1")


# def generate(state, N, L, hi):
#   res = dict()
#   ch = state.choices(L)
#   for i in trange(N):
#     for (n,cnt) in ch.stream(hi):
#       letters = state.alphabet.to_str(n)
#       poss = set([state.alphabet(''.join(c)) for c in combinations(letters, L)])
#       words = set.union(*[state[L].get(x, set()) for x in poss])
#       assert cnt == len(words)
#       res[letters] = words
#   print("num=", len(res), max(len(s) for s in res.values()))
#   return res


import flagmining.debug

state = ana_state(filename=Path(__file__).parent / 'data' / 'nsf2021.txt')

from omstokkdb import OmstokkDB
db = OmstokkDB.connect(Path(__file__).parent / 'omstokk.db')

from tqdm import trange, tqdm


def cli_gen(N=None, L=None, **kwargs):
  res = dict()
  for ls,c in gen_anasets(state, L):
    if len(res) >= N:
      break
    res[ls] = anagram_words(state[L], ls, L)

  print("Adding anagrams to SQLite DB: ", end='')
  db.add_anagrams(res)
  print("ok.")

if __name__ == '__main__':
  import argparse

  p = argparse.ArgumentParser(description='Anagram generation.')
  p.set_defaults(func=lambda **_kw: p.print_help())
  p.add_argument('--db', type=str, help='dummy not implemented')
  sp = p.add_subparsers(description='', help='', metavar='')
  bp = sp.add_parser('gen', help='generate anagram sets')
  bp.set_defaults(func=cli_gen)
  bp.add_argument('L', type=int, help="word length")
  bp.add_argument('N', type=int, help="number of sets to generate")

  args = p.parse_args()
  args.func(**args.__dict__)
