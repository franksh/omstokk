import flask

import json
import random
from pathlib import Path
import sys

from omstokkdb import OmstokkDB

db = OmstokkDB.connect(Path(__file__).parent / 'omstokk.db')

# We want:

# - length selection should feel somewhat random,
# - length should be biased toward longer words in later rounds,
# - longer words should not appear too early.

# For now we'll use 3rd degree polynomials `C_l (r-k_l)^3` for the
# weighting, ignoring negative weights, where `r` is round and `k_l` is a
# bias constant that depend on the length, and `C_l` a general scaling
# coefficient determining asymptotic behavior.

# LENGTH = 4 5 6 7  8
#      k = 0 1 3 7 11

# This prevents length 5 before round 2, length 6 before round 4, length 7
# before round 8, and length 8 before round 12.

# I.e. in round 12 we might start seeing length 8 words with a low
# probability, but this probability increases toward later rounds.
# Probability of 4-letter words should be strictly decreasing as rounds
# increase.
#
# The seed parameter was due to some other idea, of having games seeded such
# that a group of people are presented with the same word.

mcg_a = 47026247687942121848144207491837418733
mcg_m = 1 << 128

def rd_length(rd, seed=None):
  if seed is None:
    c = random.getrandbits(128)
  else:
    c = pow(mcg_a, rd, mcg_m)
    c = (c + seed) * mcg_a % mcg_m
  r = c / mcg_m
  w = [max(0, s * (rd - k)**3) for s,k in zip(
    # 4 5 6 7  8  9
    [ 1,2,4,8,10],
    [ 0,1,3,7,11])]
  for i,v in enumerate(w):
    v /= sum(w)
    r -= v
    if r < 0:
      return i + 4
  assert False


difficulties = {
  0: (1,2),
  1: (2,8),
  2: (8,16),
  3: (16,100)
}


app = flask.Flask(__name__,
            static_url_path='/static/',
            static_folder='static/')

# The secret.
secret_file = Path('flash_secret.txt')
if not secret_file.exists():
  import secrets
  with secret_file.open('wb') as f:
    f.write(secrets.token_bytes(16))
app.secret_key = secret_file.open('rb').read()
app.config.update(SESSION_COOKIE_SAMESITE='Lax')

# Starting to dislike Flask...
from flask import render_template, jsonify, request, after_this_request, make_response, session


@app.route('/')
def hello_world():
  resp = make_response(render_template('index.html', my_name=session.get('name', None)))
  return resp

@app.route('/omstokk/hs/score/<int:d>', methods=['GET'])
def get_highscores(d):
  return render_template('highscores.html', scores=db.highscores(d)[d])

@app.route('/omstokk/debrief/<int:id>')
def game_debrief(id):
  resp = make_response(render_template('debrief.html',
          ))
  return resp

@app.route('/omstokk/reset-name', methods=['POST'])
def name_reset():
  session.pop('name', None)
  return make_response('ok')


# This is just an temporary ugliness to get shit to work.
# Once it becomes annoying enough I'll change it.
def history_to_dict(ls,sols,*rest):
  res = dict()
  L,ls = ls.split(':')
  res['word_length'] = int(L)
  res['letters'] = ls
  res['answer'] = '-' if len(rest) < 1 else rest[0]
  res['points'] = '-' if len(rest) < 2 else int(rest[1])
  res['allowed'] = sorted(sols)
  return res

@app.route('/omstokk/hs/post', methods=['POST'])
def post_highscore():
  res = request.get_json()
  session['name'] = res.get('name')
  rid = db.add_score(
    int(res.get('difficulty')),
    res.get('name'),
    int(res.get('score')),
    res.get('history'),
  ).lastrowid

  resp = make_response(render_template(
    'debrief.html',
    sets=[history_to_dict(*x) for x in res['history']]))
  return resp

@app.route('/omstokk/ls/get', methods=['GET'])
def get_lettersets():
  n = int(request.args.get('n', '1'))
  rd = int(request.args.get('rd'))
  d = int(request.args.get('d'))
  assert 0 <= d < 4
  assert 1 <= n <= 10
  assert 1 <= rd <= 9999
  return jsonify(sum([db.anagrams(rd_length(k), *difficulties[d]) for k in range(rd, rd+n)], []))


if __name__ == '__main__':
  import waitress

  waitress.serve(app, host='0.0.0.0', port=int(sys.argv[1]))
