import sqlite3
from functools import wraps
import time
from collections import defaultdict
import html # just for escape
from pathlib import Path


def autocommit(f):
  @wraps(f)
  def wrapper(self, *args, **kwargs):
    res = None
    try:
      res = f(self, *args, **kwargs)
    except Exception as e:
      if self.in_transation:
        self.rollback()
      raise
    else:
      self.commit()
      return res
  return wrapper



class OmstokkDB(sqlite3.Connection):
  # 9 days, in seconds.
  SCORE_LINGER = 7 * 24 * 60 * 60.0
  LANGUAGES = dict(no=0)

  @staticmethod
  def connect(fname):
    return sqlite3.connect(fname, 5, 0, 'DEFERRED', False, OmstokkDB)

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    try:
      self.fetchall('select 1 from games, anagrams limit 1')
    except sqlite3.OperationalError:
      self.init_db()

  def init_db(self):
    self.executescript('''
    begin;

    create table if not exists games(
      game_id integer primary key,

      w real not null, -- timestamp
      language integer not null,
      difficulty integer not null,

      name text not null,
      score integer not null,
      rounds integer not null,

      failed text not null,
      history text not null
    );

    create table if not exists anagrams(
      ana_id integer primary key,

      language integer not null,
      word_length integer not null,
      letters text not null,

      num_letters integer not null, -- redundant
      num_sols integer not null, -- redundant
      solutions text not null,

      unique(language, word_length, letters)
    );

    commit;
    ''')

  def fetchall(self, *args):
    return self.execute(*args).fetchall()

  def fetchone(self, sql, default, *args):
    x = self.execute(sql, *args)
    g = x.fetchone()
    if g is None:
      return default
    return g

  def add_anagrams(self, anas, language='no'):
    with self:
      return self.executemany(f'''
      insert or ignore into anagrams(
        language, word_length,
        letters, num_letters,
        num_sols, solutions
      ) values(?, ?, ?, ?, ?, ?)
      ''', ((self.LANGUAGES[language], len(max(s)),
             ''.join(sorted(ls)), len(ls),
             len(s),
             ','.join(s)) for ls,s in anas.items()))
    self.execute('commit')

  def anagrams(self, length, lo, hi=None, n=1, language='no'):
    hi = hi or (lo + 1)
    return [[f'{length}:{k}', v.split(',')] for (k,v) in self.fetchall(f'''
      select letters, solutions from anagrams
      where language = ? and word_length = ? and num_sols >= ? and num_sols < ? order by random() limit ?
      ''', (self.LANGUAGES[language], length, lo, hi, n, ))]

  def add_score(self, difficulty, name, score, history, language='no'):
    t = time.time()
    assert 0 <= difficulty < 4
    assert len(history) >= 1
    assert len(name) >= 1
    # self.execute('delete from high_scores where w < ?', (t - self.SCORE_LINGER, ))
    with self:
      return self.execute('''insert into games(
      w, difficulty, language,
      name, score, rounds,
      failed, history
      ) values (?,?,?, ?,?,?, ?,?)''',
                          (t, difficulty, self.LANGUAGES[language],
                           name, score, len(history),
                           history[-1][0], ','.join(x[0] for x in history[:-1])))

  def highscores(self, difficulty=None, n=10, language='no'):
    t = time.time()
    res = defaultdict(list)
    # I don't know SQL so there might be way better and more efficient ways of
    # doing this.
    for (rk,d,w,n,s,rd,f) in self.execute(f'''
    with
    recent_scores as (
      select * from games
        where w >= ?
          and difficulty {" = ?" if difficulty is not None else " is not ?"}
          and language = ?
    ),
    personal_scores as (
      select (dense_rank() over hs) as p_rank, w, difficulty, name, score, rounds, failed
      from recent_scores
      window hs as (partition by difficulty, name order by score desc, w asc)
    ),
    high_scores as (
      select (dense_rank() over hs) as d_rank, w, difficulty, name, score, rounds, failed
      from personal_scores
      where p_rank = 1
      window hs as (partition by difficulty order by score desc, w asc)
    )
    select d_rank, difficulty, w, name, score, rounds, failed from high_scores where d_rank <= ? order by difficulty, d_rank
    ''', (t - self.SCORE_LINGER, difficulty, self.LANGUAGES[language], n)):
      assert rk > 0
      while len(res[d]) < rk:
        res[d].append(None)
      res[d][rk-1] = dict(rank=rk, name=n, age=(t-w), score=s, rounds=rd, failed=f)
    return res

  def debrief(self, rowid):
    '''
    select
    '''
    return None


def cli_clear(**args):
  db = OmstokkDB.connect(Path(__file__).parent / 'omstokk.db')
  print("Wiping scores: ", end='')
  db.execute('drop table games')
  db.init_db()
  print('ok.')

def cli_load(**args):
  db = OmstokkDB.connect(Path(__file__).parent / 'omstokk.db')
  import json
  with open('scores.json', 'rt') as fil:
    db.executemany('insert into games values (?,?,?,?,?,?,?,?,?)', json.load(fil))
  db.commit()
  print('ok.')

def cli_save(**args):
  db = OmstokkDB.connect(Path(__file__).parent / 'omstokk.db')
  import json
  with open('scores.json', 'wt') as fil:
    json.dump(db.fetchall('select * from games'), fil)
  print('ok.')


if __name__ == '__main__':
  import argparse

  p = argparse.ArgumentParser(description='Manage the Omstokk database.')
  p.set_defaults(func=p.print_help)
  p.add_argument('--db', type=str, help='dummy not implemented')

  sp = p.add_subparsers(description='', help='', metavar='')
  sp.add_parser('clear_scores', help='wipe all scores from the database').set_defaults(func=cli_clear)
  sp.add_parser('save_scores', help='save scores to json').set_defaults(func=cli_save)
  sp.add_parser('load_scores', help='load scores from json').set_defaults(func=cli_load)

  args = p.parse_args()

  args.func()
